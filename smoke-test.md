# Test USSD Menu (Phone)
1. Dial * 483 * 061 #

# Test USSD Menu (CLI, Bypass AfricasTalking)
1. Setup `cic-stack/cic-ussd`:

```bash
# assumes you are using a unix environment
git clone https://gitlab.com/grassrootseconomics/cic-internal-integration.git
cd apps/cic-ussd
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt --extra-index-url=https://pip.grassrootseconomics.net
```

2. Use the CLI

```bash
# assumes you are in the cic-stack/apps/cic-ussd folder
USSD_SERVICE_CODE=*483*061# python cic_ussd/runnable/client.py --host ussd.grassecon.net --port 443 -c $(pwd)/config 254700123456
```

*alternatively, the `cic-ussd-client` package is also available in [ge-dev](https://gitlab.com/grassrootseconomics/ge-dev)*

## Change language and check balance
1. English / Kiswahili / Other languages (do this entire process in various languages) 
1. Ensure you got 50 SRF tokens

## Send
1. Send to registered number
1. Send to number not registered
1. Verify both parties get a receipt SMS 
1. Check the balance of each user to verify the send happened correctly
1. Send from an account while having insufficient balance

## My account My business
1. See my business
1. Change my business
1. Check bal
1. Check mini statment

## Change Token 
1. Receive a different token than SRF (See Test Token Creation)
1. Change to that token
1. Send that token to someone else

## Help
1. Correct helpline 0757628885

## Pin Options
1. Change pin
1. Set a Guardian and ensure they can reset your pin
1. Reset pin for another user you are guardin of
1. Remove a Guardian and ensure they can't reset your pin
1. Ensure that the office numbers can reset pins

## Check user info

Install ```git clone https://git.grassecon.net/grassrootseconomics/cic-staff-installer.git```
note readme: https://git.grassecon.net/grassrootseconomics/cic-staff-installer
This includes both the CIC deployment tool and the Clicada User Infomation view
export CIC_ROOT_URL=file://`pwd`/var ### TBH..... to connect to bloxberg meta and cache from outside
export CIC_ROOT_URL=http://......

bash setup.sh

```
AUTH_PASSPHRASE=merman AUTH_KEYRING_PATH=/home/lash/src/home/python/usumbufu/tests/testdata/pgp/.gnupg/ AUTH_KEY=FXXASAS  CHAIN_SPEC=evm:byzantium:8996:bloxberg CIC_REGISTRY_ADDRESS=0xcf60ebc445b636a5ab787f9e8bc465a2a3ef8299 RPC_PROVIDER=http://localhost:63545 TX_CACHE_URL=http://localhost:63313 PYTHONPATH=. python clicada/runnable/view.py u --meta-url http://localhost:63380 +254692511395   
Phone: +254692511395
Network address: 0x8555CF8d1332243c386Cb1542Be46b0c2289F6A5
Chain: bloxberg
Name: Brent Davis (+254692511395)
Registered: Sat Nov 28 05:40:38 2020
Gender: male
Location: ruben
Products: fruit
Tags: ussd,individual
Balances:
    GFT 2.000100

Sat Nov  6 10:30:25 2021    Elke Hübel (+254681421536) => Brent Davis (+254692511395)    GFT 0.000100
Wed Nov  3 19:59:30 2021    GE => Brent Davis (+254692511395)    GFT 0.000000
Wed Nov  3 19:59:25 2021    FAUCET => Brent Davis (+254692511395)    GFT 2.000000
```

1. Verify the transaction history for a user is correct - compare to mini statments on USSD
1. Choose Another 2 users and verify their balance on the old Sempo system and new system

# Test token creation

1. Command Line: Create a new token with demurrage, name, location, description (try a demurage of 2% per minute and a suply of one million to see the effect)

### Dependencies

- [cic-cli](https://gitlab.com/cicnet/cic-cli)
- [cic-internal-integration](https://gitlab.com/grassrootseconomics/cic-internal-integration)
- [eth-erc20](https://gitlab.com/cicnet/eth-erc20) | [erc20-demurrage-token](https://gitlab.com/cicnet/erc20-demurrage-token)

cic-cli describes all the required processes to define a token, and it's attached proofs along with the means to deploy
it.

cic-internal-integration contains the microservices required to run a Bloxberg node  along with jobs needed to deploy
the base contracts against which tokens are deployed.

eth-erc20/erc20-demurrage-token contains the descriptions erc20 tokens that can be deployed on the network.


#### Setup

1. #####Clone the repositories in question.
2. #####Run the bloxberg node and deploy base contracts:

This step will run the following services under `cic-internal-integration`:

- cic-eth-tasker
- cic-meta-server
- cic-signer


```shell
cd <your-path>/cic-internal-integration/

# run the bloxberg node
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up --build evm

# deploy base contracts
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 RUN_MASK=1 docker-compose up --build bootstrap # Deploys global contracts.
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=2 RUN_MASK=1 docker-compose up --build bootstrap # Deploys instance specific contracts.
```

3. #####Generate token data template:

In a different terminal tab/window

```shell
cd <your-path>/cic-cli/

# create a virtual environment. An assumption is made that you have python-venv or python3-venv installed.
# Other virtual environment creation tools such as virtualenv would work just as well.
python3 -m venv <your-virtual-env-name>

# install dependencies
pip install -r requirements.txt --extra-index-url https://pip.grassrootseconomics.net:8433
pip install -r eth_requirements.txt --extra-index-url https://pip.grassrootseconomics.net:8433

# generate template data for an example token FOO
PYTHONPATH=. python cic/runnable/cic_cmd.py init --target eth --name "Foo Token" --symbol FOO --precision 6 <directory-to-initialize>
```

This will generate a directory matching the name specified under `<directory-to-initialize>` with the following structure:

```asciidoc
<directory-to-initialize>/
├─ attachments/
├─ meta.json
├─ network.json
├─ proof.json
├─ token.json
```

The `attachments` folder will hold any files attached to the token's creations eg. a contract guaranteeing the acceptance
of the token for some goods and or services etc.

The `meta.json` file describes mutable aspects related to the token i.e contacts of the token issuer, their country code,
location and a contact entity's name:

```json
{
	"contact": {},
	"country_code": "",
	"location": "",
	"name": ""
}
```

The `network.json` file describes data regarding the blockchain on which the token will be deployed:

```json
{
	"resources": {
		"eth": {
			"chain_spec": {
				"arch": null,
				"common_name": null,
				"extra": {},
				"fork": null,
				"network_id": null
			},
			"contents": {
				"address_declarator": {
					"key_account": null,
					"reference": null
				},
				"token": {
					"key_account": null,
					"reference": null
				},
				"token_index": {
					"key_account": null,
					"reference": null
				}
			},
			"registry": null
		}
	}
}
```
The `key_account` key refers to the `wallet-keystore` address which can be found here:
`<your-path>/cic-internal-integration/apps/contract-migration/keystore`

The `reference` key refers to the address associated with the specific attribute of the structure e.g: 
```json

{
  "address_declarator": {
    "key_account": null,
    "reference": null
  }
}
```
The `reference` here would refer to the `address-declarator`'s contract address.

The `token` attributes `reference` key value pair maybe left as null since at this point the token is yest to be deployed
as such the token's address is unavailable.

The other values required to populate these fields can be retrieved from the outputs of deploying the base contracts i.e:

```shell
...
bootstrap_1             | DEV_ADDRESS_DECLARATOR=0xb708175e3f6cd850643aaf7b32212afad50e2549
...
bootstrap_1             | CHAIN_SPEC=evm:byzantium:8996:bloxberg
bootstrap_1             | RPC_PROVIDER=http://evm:8545
...
bootstrap_1             | CIC_REGISTRY_ADDRESS=0xcf60ebc445b636a5ab787f9e8bc465a2a3ef8299
```
In order to retrieve the  `token_index` value the network can be queried for it's deployed base contracts using the 
`eth-contract-registry` library:

```shell
# install eth-contract-registry
pip install eth-contract-registry --extra-index-url https://pip.grassrootseconomics.net:8433

# query contract registry to get token index
eth-contract-registry-list -e CF60ebC445b636a5ab787F9E8BC465A2A3eF8299 -u -p http://localhost:63545
```
This will list all deployed contracts including the token registry i.e `token_index`:

```shell
...
TokenRegistry	f374d7B507767101a4bf3bA2a6B99AC737A44f6d
...
```

The `proof.py` file describes the immutable data related to the token i.e:
```json
{
	"description": "",
	"issuer": "",
	"namespace": "",
	"proofs": [],
	"version": 1
}
```

The `token.py` file described the component attributes of the token i.e:

```json
{
	"code": "",
	"extra": {},
	"name": "",
	"precision": "",
	"supply": "",
	"symbol": ""
}
```
_N/B The code section of this structure should point to the erc20 token type one intends to deploy, this should as such
reference the binary file of the token in question i.e 
`<your-path>/eth-erc20/python/giftable_erc20_token/data/GiftableToken.bin` or the numerous token flavours under:
`<your-path>/erc20-demurrage-token/python/erc20_demurrage_token/data/...`_

4. ##### [Optional] Verify described data

```shell
 PYTHONPATH=. python cic/runnable/cic_cmd.py show -d <directory-to-initialize>
```

5. ##### Deploy token

```shell
 PYTHONPATH=. python cic/runnable/cic_cmd.py export -d <directory-to-initialize> -o <output-directory> --metadata-endpoint http://localhost:63380 -y <your-path>/cic-internal-integration/apps/contract-migration/keystore -p  http://localhost:63545 -vv eth
```
1. Ensure that the token is showing up in the change token menu
1. Change to that token and check your mini statement
1. Distribute that token to a user
1. Observe that your balance of that token is dropping at the expected rate.
1. Observe that the sink account is growing at the expected rate.

# Test a batch transfer 

```
from: https://gitlab.com/chaintool/chaind-eth/-/tree/lash/sendscript

Start or connect to your node: sudo docker-compose up eth
Contract Migration: sudo COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 RUN_MASK=1 docker-compose up --build contract-migration
git clone https://gitlab.com/chaintool/chaind
cd chaind
python -m venv .venv
. .venv/bin/activate
pip install --extra-index-url https://pip.grassrootseconomics.net:8433 -r requirements.txt
```

# Test Graphana dashboard

1. Goto our graphana website .....<>?
1. Adjust filters to be able to see new tokens added.
1. Validate some transactions you just made are shown there.
1. Download a CSV of the most recent transactions and users
