# CIC Commons Charter

## Vision
The Community Inclusion Currency (CIC) Commons sees a world where communities are in control of their own currency technology as an essential ingredient for economic liberty and empowerment.

## Mission

The CIC Commons presents a collection of tools to create community inclusion currencies, and to enable interactions between any community created currency and various technologies. The CIC Commons enables people to choose and implement software that is flexible to meet their needs and comprehension. Communities can use these tools to develop assignable credit oblications on different ledger technologies and networks. In turn, they use these tools and networks to create their own applications and services that provide, connect, serve and protect the currencies they have created.

## How

- Identify and define concepts that are common across the bulk of existing cryptocurrency and community currency technologies
- Turn these concepts into low-cost, highly available, blockchain agnostic tooling for CIC creation
- Contribute to node infrastructure that can be run on any devices, especially low-end ones
- Counteract centralization of blockchain node infrastructure by making nodes as localized as possible
- Treat technology implementers as contributors not just consumers
- Reduce the cost of moving a currency systems between technologies
- Improve on the privacy properties of cash in digital currency transactions
- Create a global knowledge base of community currency implementations and best practices
- Ensure that any software services are not monopolized and that communities are empowered to run their own services

## Community Statement
The CIC Commons is an open group that identifies key roles that make up the community: software engineers, practitioners, community groups, field & support engineers, designers and artists, system administrators, web designers, writers, speakers, translators, volunteers and more.Organizations such as [Grassroots Economics Foundation](http://grassecon.org) identify employees and community bounty programs to support the CIC Commons based on avalible funding from grants. Community members are extreemly important to the CIC commons as they are the commons - your imput and support is needed to make these tools useful.

### Elinor Östrom’s Commons Governance Rules. 
 1. Who is part of the CIC Commons and who is not? What are the rules for joining? (Define clear group boundaries.)
	1. Anyone that wants to contribute to CIC Software, learnings, best practics, research implementation is welcome 
 1. Are the rules governing the CIC Commons and usage appropriate? (Match rules governing groups and the use of community resources to local needs and conditions.)
    1. Currently only a few lead architects approve pull requests and merges. They have been assigned by Grassroots Economics Foundation so far but are in the process of self defining and establishing rules for self-governance.
 1. Who can participate in modifying those rules? (Ensure that those affected by the rules can participate in modifying the rules.)
    1. To be developed. Anyone can discuss the rules for modifying the software or fork the code base. 
 1. How are CIC Commons rules legitimate outside the community or group creating them? (Make sure the rule-making rights of the community members are respected by outside authorities.)
    1. CIC Implementers like Grassroots Economics Foundation are supported to ensure security and as much legal compliance as is possible.
 1. How are CIC Commons member behaviours and rule compliance monitored? (Develop a system, carried out by the community to monitor member’s behaviour such as trade balance.)
    1. Software modififications are required to have oversight and pass test coverage.
 1. How are disputes resolved in the community? (Provide accessible low-cost means for dispute resolution.)
    1. To be developed
 1. What kind of governance layers are there for maintaining the CIC Commons? (Build responsibility for governing the common resource in nested tiers from the base level to the entire ecosystem.)
    1. To be developed

## CIC Summary
CIC is a custodial wallet and blockchain bidirectional interface engine for community inclusion currencies

- Fully Open source GPL 3.0 License
- Automates the full lifecycle of blockchain transactions
- Chain agnostic by design
- Introduces a new chain library toolset written from scratch
- Modular design with fully standalone but recombinable components
- Includes a broad collection CLI tools for most common chain and engine operations
- Empowers implementers to choose their level of abstraction
- Supports both local, containerized and decentralized environments
- Support last mile front-end apis and integrations

## System parts

- **Base components**, containing all necessary provisions for services and tooling. ([Chaintools](https://gitlab.com/chaintool))
- **Generic services components**, libraries, daemons and services making up the custodial engine. ([cicnet](https://gitlab.com/chaintool))
- **Deployment components**, which facilitates deployment of the custodial engine, as well as seeding data for development, demonstration and migration. ([GrassrootsEconomics](https://gitlab.com/grassrootseconomics/)) 


### Base components

- **Queue handling** ([chainqueue](https://gitlab.com/chaintool/chainqueue)): Makes sure that a transaction sent on behalf of a user is sent, resubmitted if stalled, and whose execution is verified by quering the network state. 
- **Chain syncing**, ([chainsyncer](https://gitlab.com/chaintool/chainsyncer)): Retrieves all state changes from the network and executes an arbitary number of code fragments per transaction. 
- **RPC (Remote Procedure Call) interface**, which divides in two parts:
	1. **Generic interface** ([chainlib](https://gitlab.com/chaintool/chainlib)): Concepts common to all chain-like RPCs, and provides a thread-safe interaction framework.
	1. **Ethereum interface** ([chainlib-eth](https://gitlab.com/chaintool/chainlib-eth)): An extension of the former for the Ethereum/EVM network.
- **Chain tooling** ([chainlib](https://gitlab.com/chaintool/chainlib), [chainlib-eth](https://gitlab.com/chaintool/chainlib-eth)): Granular access to all conceptual layers of chain interaction, including binary serializations and application interfaces, along with CLI (Command Line Interface) tooling framework.
- **Signer**, ([crypto-dev-signer](https://gitlab.com/chaintool/crypto-dev-signer)) Low-security keystore and signer which is easily usable in both production (provided that no external access is possible) and in any development environment.
- **Configuration** ([confini](https://gitlab.com/nolash/python-confini)): Incrementally merging configuration definition from multiple modules, and easily overriding them with command line flags and environment variables.


### Generic services components

- All smart contract wrappers in the [cicnet](https://gitlab.com/cicnet) repository.
- **CIC Contract registry** ([cic-eth-registry](https://gitlab.com/grassrootseconomics/cic-eth-registry)): Defines token and resource pointer lookup and authentication resources.
- Daemons and microservices in the **apps** subdirectories in the [cic-internal-integration](https://gitlab.com/grassrootseconomics/cic-internal-integration) monorepo, specifically:
	* **cic-eth**: Massively parallell and fault-tolerant implementation of the custodial signer/queuer/syncer engine, accepting tasks from end-users via middleware.
	* **cic-cache**: Cache syncer and database fetching and storing details on transactions of interest.
	* **cic-ussd**: State machine, microservices and gateway for end-users using the USSD (Unstructured Supplementary Service Data) interface via telcos.
	* **cic-notify**: Pluggable notification engine, initially used only for SMS notifications to end-users.


### Deployment components

- Data seeding and/or migrations for new development deployments. Located in the **apps/data-seeding** subdirectory of [cic-internal-integration](https://gitlab.com/grassrootseconomics/cic-internal-integration).
- Deployment and initialization of network resources (smart contracts etc) and initialization of the custodial engine. Located in the **apps/contract-migrations** subdirectory of [cic-internal-integration](https://gitlab.com/grassrootseconomics/cic-internal-integration).


## Design principles

The guiding principles for this new system are described as subsections below.


### Massively scalable

The queueing and syncing engine should be able to queue and sync state change requests and state queries to the full capacity of the integrated networks, without any loss of data and with minimum middleware latency for the end-user.


### Avoid lock-ins

.. whether by opacity, complexity or license terms.

No part of the system should rely on and/or integrate with libraries or services that cannot be reasonably understood and/or adapted.

Nor should the work required to replace a component be unreasonable large as a result of external dependencies.

It should also be straightforward for services implementers to choose their own services dependencies (like chains, chain nodes, SaaS deferrals etc).


### Lean dependency graph

The base components should make use of primitives and standard libraries as much as possible.

The services components written for Grassroots Economics specifically should only rely on generic base components.


### Network state as the source of truth

Changes to state is sent to network immediately.

State queries made through the engine always retrieve state from network before being adjusted.


### Split engine into reusable modules

Which in essence is practical implementation of the "system parts" chapter below.


### No soft lock-in to virtual environments

Too many modern software projects only provides reasonable ease of use through mechanisms like Docker or similar frameworks.

Care should be taken to incur minimum penalty for using the application and tooling in a local host environment.

Documentation must exist and be maintained to facilitate this.

Furthermore, there should exist local-only alternate services of the underlying base components. Example of this is the [chaind](https://gitlab.com/chaintool/chaind) / [chaind-eth](https://gitlab.com/chaintool/chaind-eth) pair which enables queue/sync dynamics in a local services (e.g. systemd) environment for an independent wallet.


### Coherent configurations regardless of entry-point

Merged ini-format configuration files constitue configuration schemas and overrides, translating to a configuration directive vocabulary overrideable by environment variables of the same name.


### CLI tooling framework

Layered definitions of command line arguments, so that concepts common for each layer is only defined in one place.

Full and intuitive control of overriding configuration directives with command line arguments and flags.

Easy generation of base resource objects (such as rpc, wallet) directly from rendered configurations.

Faciliate and encourage use of chainable commands (e.g. UNIX pipelines).
