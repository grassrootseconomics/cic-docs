# The DOCS have moved!

 Please visit [the new docs site](https://docs.grassecon.org)

## Community Inclusion Currencies (CICs) Documentation
Community Inclusion Currency (CIC) technology seeks to give organizations and communities the ability to issue, use and manage CICs in order to build resilient and connected economies.

 ## General Documentation
 + [White Paper](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/CIC-White-Paper.pdf)
 + [Training Materials](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/CIC_Training_Guide_Kenya.pdf) 
 + [Frequently Asked Questions](https://docs.google.com/document/d/1qtlOEL4pqW1vOL893BaXH9OqRSAO3k0q2eWbVIkxEvU/edit?usp=sharing) 
 + [Blog](https://www.grassrootseconomics.org/blog)
 + [MOOC](https://www.grassrootseconomics.org/mooc) - Very old (paper based systems)
 + [Village Market Simulator series](https://www.youtube.com/playlist?list=PLPUExzwZAUpbEInJy_8Wj_c_mDsw7-qXe)
 + [Transaction Data CSVs](https://www.grassrootseconomics.org/research): Transaction Datasets and Research
 + [Live Data Dashboard](https://dashboard.sarafu.network)
 + [Animated Explainer Video](https://www.youtube.com/watch?v=vJL9-FFleow)
 + [Jam Board](https://jamboard.google.com/d/1PcowWC7UNCOtfpaeUT2hu8_GoEU-kwzmcHYLvplUFOM/edit?usp=sharing)
 + [Preso](https://drive.google.com/file/d/1qLfNgIBkhnrxesAJd2Sn9eCxsAzctSqV/view?usp=sharing)
 
 
 ## CIC-Commons - Open source code
 + **CIC-Charter**, Charter and top level code desctiption ([CIC Charter](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/cic-charter.md))  
 + **Base components**, containing all necessary provisions for services and tooling. ([Chaintools](https://gitlab.com/chaintool))
 + **Generic services components**, libraries, daemons and services making up the custodial engine. ([cicnet](https://gitlab.com/chaintool))
 + **Deployment components**, which facilitates deployment of the custodial engine, as well as seeding data for development, demonstration and migration. ([GrassrootsEconomics](https://gitlab.com/grassrootseconomics/)) 
 + [Dashboard](https://github.com/GrassrootsEconomics/Accdash) [Dashboard API](https://github.com/GrassrootsEconomics/Accap)
 + [Demurrage Smart Contract](https://gitlab.com/cicnet/erc20-demurrage-token) 

## CIC full system notes ##
See ([CIC Engine](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/cic-engine.md)) for our vision mission and community statement
 * Blockchain
   * Currently: To ensure decentralization, censor proofing and fair gas prices we work on Humanitarian Blockchains like bloxberg.org.
     * Blockchain 1st vs database
     * Any interface can be developed without central control
     * A blockchain where gas prices can be kept to zero. 
     * A Proof of Authority chain where nodes can be elected democratically or Proof of Stake where the Token Issuers have Stake
     * A blockchain that enables easy and secure bridging to other chains. 
 * Blockchain Contracts
   * [Demurrage token](https://gitlab.com/cicnet/erc20-demurrage-token) - Network / Basic Income token - Mints tokens that include a demurrage.
   * Bridge - in order to move between the side chains (Layer 2) we use a bridge 
     * Currently: We have no connections to Ethereum Mainnet 
     * Goal: To have options to bridge to a variety of other tokens - possibly Cosmos Protocol
   * Liquidty Pools - Enable connection of Sarafu and CICs to each other and outside networks
     * Currently not in use. 
     * Goal" A variety of options for connecting CICs and other tokens, such as [Static and Bonded Liquidity pools](https://www.grassrootseconomics.org/post/static-vs-bonded-liquidity-pools-for-cics)
   * Contract Deployment (Currency Creation service)
     * Currently: We currently deploy all contracts via Command line
     * Goal: A web based interface for organizations and communities to create their own tokens and other contracts (DAO)
* CIC Token Creation system 
     * Currently: Manual contract deployment (command line) and paper based agreements with communities.
     * Goal: Registered organizations can submit an application to be an issuer, they must have:
        * an audited and legally binding backing commitment signed by local guarentors to back the total amount of a CIC .
        * The CIC issuers should also choose to add thier CIC to a liquidity pool with Sarafu or other CICs for connections
     * Longer term goal: DAOs
* Cloud server - is where we host all of our systems, Platform, Dashboard, future blockchain node.  
   * Currently: We are using Digital Ocean for instances and data storgage along with postgress databases - Transactions are done here first then synced with blockchain
   * Goal: Migrating servers to local data centers using Kubernetes. 
     * Moving toward a web-based wallet and decentralized data storage with self-soverign data (certificate based)
 * Simulations 
   * Currently: We started with a home grown Village Market Simulator and are currently are developing and using a cadCAD based [modeling](https://gitlab.com/grassrootseconomics/cic-modeling) simulation built by BlockScience
   * Goal: To build on this simulation to validate and stress test economic models and to provide predictive analysis based on data
 * Dashboard
   * Currently: [Our dashboard](https://dashboard.sarafu.network) uses a D3 javascript we also have an internal dashboard for CIC platform administrators. 
     * We are pulling data from an internal postgres database on the CIC Mgmt Platform as well as blockchain transaction data from BlockScout
   * Goal: To have system related information available to field officers and users on Android phones and to pull data from our own blockchain node.
     * To allow donors to give directly and measure the impact of their giving
     * To visualize trade and the health of economies and communities
     * To pull transaction and user meta data from blockchain directly (also used in marketplace)
 * Marketplace
   * Currently: Word of mouth but Grassroots Economics provides limited signage to users and marketing of products in the network is done by word of mouth, SMS messaging and a phone support team.
   * Goal: A web based interface for organizations and communities to share projects, event offers and wants using CICs
     * Self-sovergin [certificates](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/spec/020_redeemable_certifcate.md) (NFTs) form the basis for the marketplace 
     * A USSD marketplace (Directory) system that can be searchable and likned to regional areas
 * CIC Administration Dashboard (CICADA) - Management Platform 
     * Currently: System admins (GE staff) are able to assist users to reset pins, reverse transactions, create new users. The system is build on React and Flask in Python and uses postgres for data stroage.
     * Goal: Migration to Angular and stabilization of the platform and synchronization to blockchain, and eventual migration to a webBased wallet system with cloud storage with minimal need.
 * Wallet:
     * Currently: Users connect via USSD through AfricasTalking to connect with their wallet on the CIC Mgmt Platform, private keys are held by Grassroots Economics for these phones
     * Goal: 
       * Telegram Chat bot
       * [WebApp](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/spec/010_Web_Wallet.md) 
 * Fiat On-ramp / off-ramp - In order to enable users and donors to interact with CICs we need a way to move fiat on and off the blockchain
   * Currently: any connection to national currency is handled manually (eMoney / bank transfers)
   * Goal: To connect to / create API driven liquidity pools to fiat (eMoney) 
     * To make credit card another other payment on and offramps available and streamline our fiat to token processes.
     * Exchange Rates will be variable based on the CIC → KSH rate (depends on the liquidity pool maths) 
 * Blockchain interaction (once bridged)
   * Anyone who can obtain or create any CIC token can send it to a liquidity pool if avalaible for conversion and bridging. 
 * Data /ID System - 
   * Currently: Each user has a sim card and the Telecom provides KYC services - users are asked a pin number to access the account on USSD 
   * Goal: Creating a more robust ID system for users with and without sim cards. Possibly adding brightID social ID  - like technologies with confidence scoring 
     * Non custodial data ownership on cloud where users can specify what data they want to make avalible
     * Move toward a 'claims' based system where CIC are claims against redemption with endorsments and data and other claims can be created and endorsed - such as product offering, carbon offseting, SDG Impacts.
 * Rewards and Incentives
     * Currently: These incentive systems are manual, such as initial user air drop, referral bonuses and holding fees.
     * Currently: We are using a CIC by the name of Sarafu that is not backed by anything. Users get a Sarafu for free and groups will be able to create their own CICs backed by local goods and services and connect to eachother.
     * Goal: Any org developing a CIC can easily establish various rules and policies - these should be built into the token contracts.
 * Training Materials:
   * Currently: We have minimal documentation curriculum, games and materials for training communities and CIC issuers 
   * Goal: Better curriculum and verious multi-media materials
