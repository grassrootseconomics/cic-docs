# Token Info

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Will Ruddick <willruddick@gmail.com> (grassecon.org)
* Date: 2021.07.16
* Version: 1
* Status: Pre-draft

## Rationale
Enabeling users to query the info for a particular token. e.g. after choosing it as their active token.

## Intro 
* Users should have some information about the tokens they are using and holding

## Token-Info – User Flow:

1. A user is sent or given a new token for the first time - even during registration or when choosing to change token.
1. The user gets a USSD message showing the following info:
1. Token Info:
   1. name, 
   1. symbol 
   1. holding fee (% aggregated to monthly - calculation needed based on data from contract), =(1-(1-Demurage)^43800) (43800 munites ina  months) ... if demurrage is 0.00000046124891913883 ... then the monthly demurage here is  =(1-(1-0.00000046124891913883)^43800)=0.02 ... 2%  ... 
      1. note that to calculate a minute by minute demurrage based on a monthly target demurrage of 2% =1- (1-0.02)^(1/43800
) = 0.00000046124891913883
1. Stats:
   1. total supply, (from contract)
   1. Circulation (total minus the supply of balance of the issuer) (from contract and balance of sink account)
   1. Number of token holders (calculated: advanced not for mvp)
   1. GINI Coefficent (against all holders including the issuer balance) (calculated: advanced not for mvp)
1. Issuer Info (this is the sink account holder)
   1. Name 
   1. phone number 
   1. product offering
   1. location

## ex:
1. Give It Up For Sally, GIUFS, holding fee: 2% monthly, 
1. Supply: 100,000, Circulation: 20,000
1. Sally Chama, +254727865533, Group Farm, Machakos


## USSD Menu

1. Token Info (can be selected under Help)
The info also comes whenever a new token is send to a receipent for the first time.


## Testing
1. Check info is correct


## Action items

## Implementation

A service would need to update this information daily? for each token.

### Workflow

### Variables

### Interface



## Security
1. Liquidity pool supply and value Alerts
1. For bonded pools: Whenever the token is converted to another token and the price changes more than 10% an alter is sent to the token issuer
1. For Bonded Pools: 1. Message Alert your excahnge value of TOKEN_NAME tokens have changed in +X% / -X% value 
1. Note that the receipt messages for token transfers that include a conversion should also show the tokens exchange value 


## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
