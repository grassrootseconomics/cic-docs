# CIC Management Platform Spec

This document describes the components and steps involved to create a CIC management platform.

## TAGS

1. Basic MVP Functions
   1. Pin Reset. -> eventually USSD/Wallet Social Pin Reset (set friend(s) as guarantors)
   1. Delete / Cancel users
   1. Create new users
   1. Searching for a user (clickable to see trades and other data)
   1. Change user details, roles (group), locations, default tokens, auto-conversion
   1. Disburse and reclaim tokens
   1. Convert tokens
   1. View transactions for whole system as they come in
   1. View transaction details of a user (clickable / searchable) lists / dashboard
   
2. Advanced
   1. Assigning admins/viewers ....  permissions roles, phone support / super admin ...  (see spec)
   1. Send to external wallet address (can be CLI)
   1. Special tables (sortable for volume, balances, pin reset needs)
   1. Mpesa integration 
   1. Airtime integration 
   1. Airtime integration 
   1. Donor integration (Donor interfaces)
   1. Hide PII data from Viewers - In general data protection
   1. Enable KYC - storage of user data and photos (ID) and limit system usage without proper KYC
   



## RATIONALE

Enable the management and customer support personnel of the CIC platform to track platform usage by end users and provide troubleshooting capabilities.


## OVERVIEW

The system gives the personnel various functionalities that allow them to be able to view and control the CIC ecosystem.
They have to be involved in all parts of the user's interaction with the system in order to be able to give timely assistance.

Some functionality enabled by the system are:
* Register personnel and assign permissions.
* View all user in the system and be able to create new users.
* View and change user details, delete users or reset user pin.
* Disburse and reclaim tokens from users and convert tokens on behalf of users.
* View all transactions and individual transaction details.

## USER STORIES

* Register personnel - Give an interface for personnel to register (invite only is fine) or sign in if already a user.
* Assign personnel permissions - A super admin will be able to assign permissions to other personnel based on their clearance level (Viewer | Enroller | Admin | Super Admin) pursuant to the [Platform Roles Spec](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/spec/004_Platform_Roles.md).
* View users - Get all users in the system and filter according to user categories.
* View user details - View a user's details and all their transactions.
* Create new users - Add users to the system and set user type - see [Platform Smoke Testing Spec](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/spec/005_Platform_Smoke_Testing.md), User Types section.
* Set user token - Create a token for Chama accounts only (requires admin privileges) or set a user's community token - see [Self Service Token Spec](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/spec/006_Self_Service_Token.md), token creation and join token sections.
* Change user details - Set new details for users such as roles (group), locations, default tokens, auto-conversion, e.t.c.
* Delete users - Delete or cancel users.
* Reset user pin - Change a user's pin on request. Eventually the user will be able to set a recovery account with which the pin can be reset from (preferably a friend or family member as a guarantor). 
* Disburse and reclaim tokens - Disburse tokens to users on registering for the platform, referrals or meeting certain parameters and reclaiming tokens for dormant accounts - see [Platform Incentives Spec](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/spec/001_platform_incentives.md).
* Convert tokens - Convert a user's tokens from one community token to another on request.
* View transactions - Get all transactions  in the system and filter according to the transaction category.
* View transaction details - View an individual transaction, and the users who participated in the transaction.
* Donor interface - A dashboard showing platform usage metadata such as transaction volume and user registration against time. The dashboard will contain public data only.
* Mpesa integration - Initiate transactions from and to Mpesa allowing for minting of new tokens by adding to the reserve or redemption of tokens for local currency.

   
### CODE

### SCHEMA


### SEMPO PLATFORM INTEGRATION

### RESOURCES

