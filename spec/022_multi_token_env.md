# Multi Token Environment (Vouchers Menu)

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Will Ruddick <willruddick@gmail.com> (grassecon.org)
* Date: 2021.05.29
* Version: 1
* Status: Pre-draft

## Rationale
Enabeling users to hold and trade multiple tokens- will allows various chamas and token issuers to be connected to eachother as well as other tokens such as rewards.

## Intro 
* Today users only hold one token aka Sarafu - created by Grassroots Economics. This means that users can't interact with other tokens at all.
* We want users to create and be able to interact with other CICs and tokens.

## Multi-Token – User Flow:

1. A users is given (via trade or token creation) multiple tokens (These tokens must all be registered on Sarafu Network)
2. The user should be able to see these tokens and their balances (if non-zero) in a 'My Tokens' Menu item (top level) - as many as possible on USSD with the highest balalances at the top.
3. The user should be able to switch which token is their Active Token. This will be used for sending, as well as default balance checks and mini statement. 

## USSD Menu: 
Top level menu item called: Vouchers (Voucha in Kiswahili)

### 1 (advancedmenu)
Type the symbol for the voucher you want to choose:
0: Back

#### XYZ


### 1 (advancedmenu)
Choose Voucher: (Chagua Voucha)
1. Sarafu 100
2. YOMA 50
3. Afya 223

0: Back

### 2
Your voucher will be set to:
([Token info](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/spec/024_token_info.md) for XYZ displayed here) 

Please enter your PIN to confirm: 
     
0: Back
9: Main Menu

### 3
Success! All transactions will now be in XYZ.

0: Back
9: Main Menu


### Main Menu
Balance 223 XYZ
1. Send
2. Help



### CICADA Mgmt Platform - GUI
1. On the user info page you should be able to set their Active Voucher - via a drop down
1. You should be able to see balances in all the tokens/vouchers they have

## Active Token 
1. Each user needs to be assigned a default active token/voucher - they start with Sarafu by default.

## (not used)  Accept other Tokens
1. If this is on and someone attempts to send another token (other than their active token) to that user the sender will recieve an error as well as the reciever similar to the Balance Limit Error https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/spec/019_wallet_soft_limit.md

## Interfaces - UX

### Command Line - CLI Set Active Token
1. Inputs: User ID and Name or ID of the token - ensure it is a valid token (ability to batch this with csv)


### USSD feature phones - Set Active Token
- New user dials ussd session code where she selects -) Vouchers
- Their Voucher sorterd by balances they have in their wallet are shown with an option to select the number of the token they want to choose
- Confirmation message and entry of pin number to Confirm
- Future transactions send from this account will try to send that token

## Testing

## Action items

## Implementation

### Workflow

### Variables

### Interface



## Security
1. 

## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
