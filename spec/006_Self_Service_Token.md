# Current Platform Incentives spec

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Will Ruddick <willruddick@gmail.com> (grassecon.org)
* Date: 2020.05.18
* Version: 1
* Status: Pre-draft

## Rationale
Enabeling users to create their own tokens is one of the main goals of CICs 

## Intro 
* Today users only receive tokens created by Grassroots Economics - the reserves of these tokens are donors giving to Grassroots Economics. 
* This means Users have very little ‘feeling’ of reserve depletion because it is not their token and not their money behind it.
* We want users to be able to create their tokens to create more ownership and diffusion.
* A CIC primarliy represents the goods and services committed by the Issuer - which should be a Group Account.
* The CIC has an enforced X% reserve amount.

## Inputs

1. User ID (must be a chama account)
2. Token Long Name - 128 character limit
3. Token short name no more than 6 Character - must be unique
2. Reserve amount - amount of tokens they want to put in reserve (minimum upon conversion 100k xDAI)
3. Number of chama members (NCM) (before we enable conversion of the new tokens there must be at least this many token members with a positive token balance)
4. Token Backing Statement: 256 Character limit - What is the off-chain guarentee behind these tokens
5. Defaults: 
   1. 25% Target Reserve Ratio (1:1 with reserve)
   1. Minimum initial reserve is $100 USD = 100 xDAI in value (of some inital token such as Sarafu)
5. These should all be added to the Token table information


## Token creation process - Backend 

1. Ensure TOKEN_Name is unique
2. Generate a token and converter (adding it to the registry) on xDAI and store private and public keys
   1. Mint the standard amount of tokens. (4x the reserve = 40,000)
   1. Convert the existing user's tokens into xDAI and put them into the converter as reserve. (min 10,000) (any remaining tokens in their account will also converted to TOKEN_NAME)
   1. The new token should have a value of 1:1 with xDAI and be at 25% reserve (Target Reserve Ratio)
1. Put the TOKEN_NAME tokens into the Users wallet (Group wallet) 
1. The token owner should not be able to transfer more tokens than he has committed to the number of token users specified.
1. The token owner should have his community token for auto-convert set to TOKEN_NAME
1. This account is a GROUP account. Meaning that only it can cash out 50% a month from it’s own reserve - along with other Chama rules.
1. If the token has been created send the SMS for the token creation approval to the creator. Else send error.

## Join Token 
2. Each user needs to be assigned a default / community token - they start with Sarafu by default.
2. Users need to be able to change tokens - but after a grace period of 1 month. 
3. Users on USSD should not be allowed to join Sarafu or the reserve xDAI as their community token. Admin accounts like the GE Agent will still be in Sarafu as their community token

## Auto Join Token
1. Users with Sarafu as their token will automatically join a Community Token of a user they trade with (given the recipient is not in Sarafu) - This is a one time event

## Auto convert
1. We want to avoid having multiple tokens in user wallets
1. When a user recieves a token we automatically try to convert that token into her Community token
2. We should be able to turn this feature off for selected users (like admins)
3. If users have multiple tokens in their wallet - it is a sign conversion failed - we need some process to retry failed conversions and to altert admins

## Manual convert
1. For admin accounts we may want to convert from and to specific tokens via the Managment Platform
2. Admins may also want to convert a users tokens who are failing to auto convert

## Join Token Interfaces - UX

### Command Line - CLI Join Token
1. User ID and Name or ID of the token - ensure it is a valid token

### Mgmt Platform - GUI
1. On the user info page you should be able to set their Community Token - via a drop down

### User interface - USSD
1. Under the My Account Menu - Join Token becomes an option
2. THey must type the token name
3. Receive a confirmation screen
4. Receive a success or failure screen


## Token creation Interface - UX

### Command Line - CLI
1. Process Inputs on command line
5. Confirmation please type the token name to proceed


### CIC Mgmt Platform - GUI
1. On a user account there should be a button for Create Token - possible for Chama accounts only) - this required admin provlages
2. The next page comes up with the inputs above
5. Confirmation please type the token name to proceed

### USSD feature phones

1. Existing User calls Sarafu
   - New user dials ***483****46# where she selects 
   - My Account -> Create Token
   1. Token Name (limited to 9 characters (auto converted to lowercase no special letters) (TOKEN_NAME)
   2. Reserve amount (the minimum is 10,000 Kenyan Shillings in value- higher amount means a higher price of the final tokens) - Max is the user’s current balance. Note that this10,000 Kenyan Shillings can be in the form of other tokens and converted to Sarafu.
   3. Token Backing Statement: What goods or services will be accepted for these tokens?
   4. Please confirm that you will accept (4x Reserve) of TOKEN_NAME's for (goods or services)? 
      *. Yes / No (if no -please contact Office 0757628885)
   4. How many people are in this chama that will recieve (Token_Name) - Minimum 10 - Note your account will be locked until you've sent out all all your tokens to this many users.
   5. Note that members must ‘join’ the token (via USSD) using the TOKEN_NAME to receive this.
4. Pin 
   * “Enter PIN
   * _______________” 
   * Repeat Pin 
   * “Enter PIN again  
   * _______________”
   * 
   * Pop Up: 
   * “Your TOKEN_NAME token is being created. 
   * You’ll receive an SMS when your token is ready.”  
   * 
   * Error:
   * If the user doesn’t repeat the same password pop up with the text 
   * “Wrong PIN, please try again” 
   * 
   * Clicking back will lead to Repeat Pin screen
5. “You will receive an SMS when your TOKEN_NAME token is ready”
6. Approval
   * At this stage, the wallet creation starts confirmation SMS will be sent only when the Backend process (see below) is over.
   * If token creation succeeds: 
   * Send an SMS to the user with the following text 
   * “You have just created the TOKEN_NAME token!
   * Dial *483*46# to use TOKEN_NAME token
   * Tell your members to JOIN TOKEN_NAME via My Account”
   * If account creation fails: 
   * Send an SMS to the user with the following text: 
   * “Whoops! There was a problem creating your token. To try again Dial *483*46#. If issues persist contact support: 0757628885”


## Exahnge changes
1. For users with their own tokens when they cash out they are doing it against their own token's reserve.
2. Also when people buy in they are adding to theri own token's reserve and minting new tokens.
3. Since GE is the intermediary for Kenyan Shillings Mpesa - sending TOken X to the GE Agent Account should first convert that token to 
   it's own reserve and give back that much Mpesa (limited by chama rules)
4. When a user sends mpesa to us - they can mint whatever their current currency is.



## Action items

## Implementation
1. Having multiple tokens touches on many of the existing system - such as receipt messages, introduction messages for new users and so on.




### Workflow

### Variables
1. Price change alert levels
2. TRR for Self Service Tokens (SSTs) 0.25
2. Conversion fee fos SSTs 0.005 (0.5%)
3. Minimum starting reserve 10,000 xDAI
4. Minimum number of chama members (conversion will be locked until the new tokens are sent to this many users)
4. Token Name
5. Number of users that must join the chama and receive tokens
5. Auto-Conversion should by default change all tokens in a user's wallet into their community token. 
   * Note that a user with Sarafu in their wallet will shift their community token to whomever they trade with that is not in Sarafu. (Sarafu is just a temorary token)
6. Grace period - Users can't join another token for 1 month after having joined another. (note that *Auto Join Token* should not count as joining a token in this regard)

### Interface


## Testing
1. Ensure conversions are working between tokens
2. Test out auto-conversion - one user gets sent a foreign token ... try to convert
3. The UX process and various limits - like minimum reserve
4. Ensure that any Sarafu or other token are converted to xDAI 
5. Chama cash out limits remain the same - but instead of sending a GE Agent Sarafu they are sending their own token

## Security
1. Token Value Alerts
1. Whenever the token is converted to another token or xDAI and the price changes more than 10% an alter is sent to the token issuer
1. Message Alert your excahnge value of TOKEN_NAME tokens have changed in +X% / -X% value 
Note that the receipt messages for token transfers that include a conversion should also show the tokens exchange value (this was on the old USSD system)


## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
