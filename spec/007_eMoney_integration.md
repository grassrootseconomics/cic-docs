## Notes ##
Users have no way to automate the excahnge National Currency -> CIC. 


## Solution ##
Using AfricasTalking API to automatically: Using RPC endpoints and contract calls to automatically send CIC when KSH is received (MPesa call back triggered)

A float pool can be created holding sellable CIC

- Mpesa -> CIC
   - Mpesa are added to our float Mpesa account.
   - CIC from our Sarafu float account is sent to the user (1:1) rate



### USSD Interface ###

- Option #4 Buy Voucher  
    - Shows the paybill where you can send mpesa to. 
   
### Security ###
 - Balance alerts: CIC balances: The account from which Sarafu is sent needs to send a sms and email alert when low 
 - Failure alerts: If the API session fails an sms and email alert should be sent
 - Reject on not enough Sarafu balance: If there is not enough Sarafu (on incoming Mpesa) the transaction should be rejected and an email sent to admin along with a SMS to admin and user (Mpesa should not be taken from user). (message should be visible also on mgmt platform txn view and user view and admin view).

