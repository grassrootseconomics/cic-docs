# Public Viewer 

1. View public data only and no ability to edit (Graphana and NFTs)

# Private Viewer

1. View private data and no ability to edit

Private data inlcudes, names, phone numbers, location (Private Graphana and CLICADA)

# Enroller

1. Inherit Private Viewer
1. Edit user fields (would be great)
1. Pin reset 

# Admin

1. Inherit Enroller
1. Assign Enrollers

# Super Admin

1. Inherit all
1. Assign the roles (Super Admin, Enroller, View Only)

