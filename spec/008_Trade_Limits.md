# Trade Limits

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Firstname Lastname <email> (url)
* Date: YYYY.MM.DD
* Version: 1
* Status: Pre-draft

## Rationale
<!--
Why are you proposing this?
-->
Various incentives as well as KYC requirments for us to limit trading to users until they pass certian strandards. 

## Before 
<!--
What is the current state of the described topic?
-->



## After
<!--
How will things be different after this has been done?
-->
### All User Limits
1. KYC LImits for max amount of outward trade per week for 
   1. phone verification and all info filled out = 20,000 Tokens
   1. National ID number verification (via api) = 50,000 Tokens
   1. Photo of Person and them with National ID = 250,000 Tokens


## Implementation
<!--
Here is the description of how these changes should be implemented.
Please use subheadings to improve readability.
Some suggestions:

### Workflow

### Variables

### Interface
-->

## Testing
<!--
Please describe what test vectors that are required for this implementation
-->

## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
