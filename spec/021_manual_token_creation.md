# Manual Token Creation spec

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Will Ruddick <willruddick@gmail.com> (grassecon.org)
* Date: 2021.05.11
* Version: 2
* Status: Pre-draft

## Rationale
Enabeling users to create their own tokens is one of the main goals of CICs. This spec goves of the token creation process 

## Intro 
* Today users only receive Sarafu created by Grassroots Economics
* This means Users have very little ownership or feel the need to accept or back the token.
* We want users to be able to create their tokens to create more ownership and diffusion.
* A CIC primarliy represents the goods and services committed by the Issuer.

## Sarafu Network – Sarafu and New Token Minting – User Flow:

1. Users create a token issuance agreement (for Sarafu or their own token) [(Sarafu Network Application)](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/Sarafu_Network_Member_App_-_Draft__en_.pdf)
2. The token creation application is audited and signed by all parties (paper form)
3. All users in the agreement dial the USSD code and setup an account (with 0 initial balance for new accounts). If they have existing tokens those will simply stay there in their account. *Note that Users with multiple tokens in their account should have an option to switch their active token to trade the other tokens. (see milti-token-env spec)
4. GE creates the new token (or mints more Sarafu) and sends it to the users as per the agreement. Also changing their Active token to the new token if one was created.
5. n.b. Initally conversion between tokens will not be allowed 

## Inputs

1. Signed Membership Application
1. if not Sarafu
   1. Accounts (walllet address) of all users in membership application
   1. Token Long Name - 128 character limit
   1. Token short name no more than 6 Character - must be unique
   1. A maximum supply should be set to the token supply.
   1. Demurrage set to accumulate to 2% a month
1. Token Supply to be minted
1. All blockchain wallet addresses in application


## Token creation process - Backend Command line and In-Person process

1. Ensure TOKEN_Name is unique
1. Create / mint the token supply
1. sarafu-token-deploy 
1. eth-token-index-add
1. eth-address-declarator-add
1. to discover the contract addresses on the network you use:
   1. eth-contract-registry-list
1. Put the TOKEN_NAME tokens into the Users wallet (as per agreement)
1. All members in the agreement should have their active token set to TOKEN_NAME (see multi-token spec)

## Token creation Interface - UX

### Command Line - CLI
1. Process Inputs on command line
1. Confirmation please type the token name to proceed
1. Ability to send specific ammounts of the token to addresses in the agreement (csv)

### CIC Mgmt Platform - GUI
1. On a user account there should be a button for Create Token - - this required super admin privlages
2. The next page comes up with the inputs above
5. Confirmation please type the token name to proceed



## Testing

## Action items

## Implementation

### Workflow

### Variables

### Interface



## Security
1. 


## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
