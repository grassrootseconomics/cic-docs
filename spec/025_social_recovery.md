<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Louis Holbrook <dev@holbrook.no> (https://holbrook.no), Philip Wafula, Will Luke
* Date: 2021.10.23
* Version: 1
* Status: Pre-draft

# Social recovery of accounts

Social recovery for the custodial system introduces another new service, `cic-eth-recovery`.

The service interfaces with a multisignature lock smart contract, and provides the following:

- a mapping of one or more wallet keys to the custodial private key
- a proxy for generating multisignature triggers for cic-eth-recovery to change existing wallet key mappings
- a proxy for generating a salt to override the safety grace period of a triggered wallet key mappng change

The service has an HTTP interface, which submits transaction calls to the cic-eth-tasker.


## Nomenclature


### Custodial private key

A private key that holds value, generated and managed by the custodial system.


### Address

The corresponding address of a custodial private key


### User

A human being for whom a custodial private key is held by the cic custodial system.


### Client

A user using a particular device to interface with the custodial system and the CIC network.


### Wallet key

A public/private key pair generated on a client device. The wallet key is used as autentication and authorization to perform queries and transactions using the user's custodial private key.


### Seed phrase

A human readable phrase that may be used to regenerate a wallet key. 


### Password

A human generated string protecting the private portion of the wallet key on a device.


### Recovery custodian

A user who may generate a valid signature for the multi signature lock contract that can trigger wallet key changes for another user.


### Social recovery id

Represents all recovery custodian signatures 


### Social recovery token

Represents a recovery custodian's signature for a particular user's custodial private key. It is the hash the recovery custodian's address and the user's address.


### Recovery request

The multisignature lock contract state when a quorum of signatures has been reached to satisfy given criteria for changing a wallet key.


### Recovery grace period

A time period that must lapse after a recovery request until a wallet key change can be made. The wallet key may cancel the recovery request at any time during this stage.


### Recovery execution

The funds held by the lost custodial private key are transferred to the new custodial private key.


## Procedure

In the following narratives, Alice is the user whose wallet keys have been lost, and Bob and Carol are benevolent recovery custodians for Alice. Mallory and Trudy are malicious recovery custodians, or are attacking the recovery system in other manners.


### Creating a recovery lock

1. Alice selects "manage recovery" on her device.
2. Alice selects "add custodian" on her device.
3. Alice enters the wallet key of Bob and submits
4. `cic-eth-recovery` calculates and social recovery id `SRID` = `hash(Aa, Sr)` and a social recovery token `SRT` = `hash(Ab, SRID)` where:
	- Aa is Alice's address
	- Ab is Bob's address
	- Sr is a salt value 
5. `cic-eth-recovery` registers a multisignature recovery lock entry with the smart contract using `SRID` and `SRT`.
6. Step 2-5 are repeated for Carol.
7. Step 2-5 are repeated for any further recovery custodians Alice wish to register, for a total of `n`
8. Alice selects "activate recovery" on her device, choosing:
	- Quorum; Which `m` of `n` recovery custodians must sign before a recovery request will be triggered
	- How long is the recovery grace period `G`
	- A easy-to-remember override phrase `Po` to trigger recovery execution before `G` has expired.
9. `cic-eth-recovery` calculates the social recovery override proof = SROP = `hash(Po, So)`, where `So` is a salt value different from `Sr`.
10. `cic-eth-recovery` registers the quorum, grace period and `SROP` against the `SRT` in the smart contract.
11. `cic-eth-recovery` _activated_  the multisignature recovery lock. From this point Bob, Carol and any other recovery custodian may initiate recovery requests.


### Deleting a recovery custodian

1. ...


### Recovering the custodial private key

Assume that `2` of `m` signatures are needed to trigger a recovery request.

1. Alice creates a new wallet key.
2. Bob selects "manage custodies"
3. Bob selects the entry for Alice, with the corresponding SRID.
4. Bob enters Alice's new wallet key.
4. Bob selects "request recovery"
5. `cic-eth-recovery` sends a signature transaction for bob to the multisignature lock contract containing the SRID and Alice's new wallet key. (signature validity is checked against `SRT`)
6. `1` of `n` signatures have now been registered.
7. The steps 2-5 are repeated for Carol.
8. `2` of `n` signatures have now been registered, quorum is reached.
9. `cic-eth-recovery` issues a notification to all Alice's devices associated with the SRID, saying a recovery request has been triggered.

(Note that Alice may already transact using her new account, while waiting for the old account to be recovered).

From here on two scenarios are possible:


#### Recovery after grace period

10. The grace period elapses
11. Alice selects `claim account` on her device.
12. `cic-eth-recovery` finalizes the multisignature lock for the SRID.
13. `cic-eth-recovery` triggers recovery execution.


#### Overridden grace period

10. Alice selects `claim account` on her device.
11. Alice enters the override phrase.
12. `cic-eth-recovery` calculates `hash(Aa, So)`
13. `cic-eth-recovery` triggers recovery execution.



## Contract interface

- `register(SRID, SRT)`
- `activate(SRID, G, SROP)`
- `request(SRID)` (where contract hashes sender address to match `SRT`)


## Attack vectors

- Stolen device
- Dictionary attack for `Po` (through `cic-eth-recovery`)
