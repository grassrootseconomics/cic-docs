# Token Conversion

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Will Ruddick <willruddick@gmail.com> (grassecon.org)
* Date: 2021.05.29
* Version: 1
* Status: Pre-draft

## Rationale
Enabeling users to convert between them various tokens should make usage easier between villages and also out to 3rd party tokens

## Intro 
* Today users only hold one token aka Sarafu (and eventually they will hold multiple tokens) but they can't convert between them
* We want users to be able to choose to convert and even auto-convert between various tokens 
* We also would like supporters to be able to contribute toward liquidity pools to support communities

## Multi-Token – User Flow:

1. A users is given (via trade or token creation) multiple tokens (These tokens must all be registered on Sarafu Network)
5. Conversion 
   1. MVP - Initally conversion between tokens will not be allowed (see conversion below)
   1. If Auto-Convert is selected then all tokens that are able to convert to their Active token will automatically attempt to convert as much as possible to that token. Note that not all tokens will be convertible and conversion in some cases might involve changing rates.
   1. The office with CICADA can help to convert tokens in future for users.

## USSD Menu

1. My Sarafu (top level)
   1. Show tokens
   1. Set Active Token
   1. Convert Tokens
   1. Auto-Convert Tokens (on/off) default is off 
   

## Conversion 

### Manual Conversion
Note that GE may also act as a manual exchange by holding a supply of several tokens. This would need both CLI and CICADA support.

### Liquidity Pools
If users want automated conversion between tokens, then a pool must be created. With a supply of any two tokens on the network that they want to convert to – a static (1:1) liquidity pool connecting those tokens will be created with that given supply of each tokens. 

If the amount in the liquidity pool is insufficient for a trade – the sending user will get an error message and not be able to trade. This should also alert the owner of the liquidity pool – and possibly trigger a community process to refill the pool. 

### Conversion User Flow
User Bob chooses to send 40 B tokens to Sally who has S as her Active token. 
The system 1st checks to see if there is a liquidity pool (or path between pools) with sufficient supply to 
give back 40 S ….. if less than 40 S are available (or there is a bonded pool) it will tell Bob the most he can receive (quotation and he could specify the minimum he wants in return to allow an exchange). 

If there is sufficient liquidity in the pool(s) then his 40 B will be added to the pool and 40 S removed and given to Sally. 

## Liquidity pool creation process 
1. GE holding 2 tokens will add a supply of both to a liquiditiy pool with a 1:1 excahnge rate. 
1. The pool needs to be added it to the registry
1. A static pool will/can become empty on one side and will stop to function in that director of conversion


## Auto convert
1. We want to avoid having multiple tokens in user wallets
1. When a user recieves a token we automatically try to convert that token into her Active token
2. We should be able to turn this feature off for selected users (like admins)
3. If users have multiple tokens in their wallet - it is a sign conversion failed - we need some process to retry failed conversions and to altert admins

## Manual convert
1. For admin accounts we may want to convert from and to specific tokens via the Managment Platform
2. Admins may also want to convert a users tokens who are failing to auto convert


## Testing
1. Ensure conversions are working between tokens when liquidity pools exist and not when they dont
2. Test out auto-conversion - one user gets sent a foreign token ... try to convert


## Action items

## Implementation

### Workflow

### Variables

### Interface



## Security
1. Liquidity pool supply and value Alerts
1. For bonded pools: Whenever the token is converted to another token and the price changes more than 10% an alter is sent to the token issuer
1. For Bonded Pools: 1. Message Alert your excahnge value of TOKEN_NAME tokens have changed in +X% / -X% value 
1. Note that the receipt messages for token transfers that include a conversion should also show the tokens exchange value 


## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
