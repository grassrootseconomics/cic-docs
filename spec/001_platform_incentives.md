# Current Platform Incentives spec

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Will Ruddick <willruddick@gmail.com> (grassecon.org)
* Date: 2021.02.23
* Version: 1
* Status: Pre-draft

## Rationale
The current Sarafu system seeks to maximize circulation in order to fill market gaps and help CIC users to support eachother.

## Currently - bonding CIC issuance to a reserve with no market value.
Grassroots Economics created a pool of 16 Million Sarafu tokens (which they value 1:1 with Kenyan Shillings ala buy-backs - see below). 
The following are all rules are platform (statemachine) based (not blockchain contract based). Note that these rules are for the Sarafu CIC only and are not meant to be the rules for other CIC creators.
1. Outgoing Sarafu
   1. **airdrop-full-profile**: New users that have filled out their profiles (name, business, location, gender) get 100 Sarafu (Shown as Disbursement in database)
   1. **usage reward**: Users get weekly Sarafu bonues if they have made any trade  that week.(Shown as Disbursement in database (transaction meta-data)) - this is a redistribution of the demurrage amount
1. Incomming Sarafu 
   1.  **holding fee/demurrage**: Users get a monthly deduction of 2% of their Sarafu balance - which is added to the user reward pool.  (Shown as Reclaimation in database). 
   1. **minting/creating**: 100 Sarafu is minted (supply increase) when new users join and are validated


## Implementation

### Workflow

### Variables

### Interface


## Testing
<!--
Please describe what test vectors that are required for this implementation
-->

## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
