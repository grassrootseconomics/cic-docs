# Redeemable Certificate Contract spec
A generic redeemable certification contract that records certification information as well as forms of evidence and validations (credentials) which  can be interacted with by a marketplaces and redeemers that seek to use the information and or reward the recipient for the certificate. In effect this certificate can act as its own ledgar - and used by marketplace applications that connect clients / rewarders with certificate holders.

We are working with vulnerable communities and individuals to accredit them for noteworthy activities: Supporting their community, working on environmental programs, passing an online course and selling local products. 

#Certificates:
  * These accreditations or certificates should be self-sovereign (owned by the receiver) deletable and non-transferable and able to be endorsed or validated by individuals and organizations. 
  * The owner should be able to choose who can read the certificate as they may contain private information and large datasets of credentialing information.
  * Viewers should be able to support the owner and write their support onto the certificate itself. 

#Marketplace:
  * Certificate owners could choose to share with a marketplace with the role to aggregate and provide indexes and assessments against them - while allowing for 3rd parties to support certificate holders.

# Example 1 Course Certification (Proof of Action)
After completing an online course the course creator issues a Certificate to the student and endorses it. 
The student can accept the certificate by signing it. The student can also delete it. The student can also choose who can see the details of the certificate, which can include credentals, course topics, as well as any txhashes or other data references.
A optional value in some currency can be placed on the certificate – this can be based on the duration of the course, cost of internet and so on.
The student may allow certian individuals or marketplaces to view the data (such as private informationa nd credentials) in the certificate. Anyone can redeem the Cert. Say a business offered the student a voucher for their store with a stated value in some currency. If the Student executes an AcceptRedemption the interaction will be appended to the certificate which can be seen by future redeemers and marketplaces.

# Example 2 Product Certification (Merchant Star Ratings)
A merchant selling shoes creates a certificate claiming he sells shoes and self signs/endorses it and accepts it.
The merchant can also choose who can see the details of the certificate (or it can be made fully open), which can include where the shoes are made, how, brands etc. as well as any txhashes or other data references.
A optional value in some currency can be placed on the certificate – this can be the price of the shoes he is selling.
Anyone can endorse or revoke endorsment on the certificate - these endorsments can be processed for a rating system using in a marketplace. 
Anyone can redeem the Cert. Say somone buying shoes. Is the merchant executes an AcceptRedemption the interaction will be appended to the certificate which can be seen by future redeemers and marketplaces.

# Usages
* Personal Data: ex. Certifying that the recipent is a mother.
* Merchant Data: ex. Endorsing a merchant (start ratings) after purchasing a product.
* Enviromental Data: ex. Certifying that the recipent is a farmer who is mulching their land or using regnerative practices. Soil quality testing etc.
* Transactional Data: ex. Certifying that the recipent is paying for school fees. The school could be a validator. If using CIC the transaction hashes themselves could add evidence.

# Schema
1. Issuer: <wallet address>
1. Issued to: <wallet address>
1. Acceptance: <signature of receiver>
1. Date Issued: 
1. Viewers: [] (who can access/read the details of the Cert.)
1. Details: [] (note that no Public Identifying Information (PII) should be here - most data will be on decentralized storage accesibile to those {viewers} with view access given by the owner))
   1. Link to details/credentials: 
      1. Info: {description:’’, duration:’’,topics:’’, quality:’’} (non-PII)
      1. Location: {GPS, Country, Area} (non-PII)
      1. Transactions: []
          1. Description of data/transactions
          1. Hashes: []
      1. Suggested Value:
          1. Currency Name
          1. Currency <contract address>
          1. Suggested Value (amount of currency specified)
1. Validations/Endorsments: [] *appendable*
   1. Validated by
        1. Name
        1. Website
        1. Wallet address
1. Redemptions/Purchases: [] *appendable*
   1. Redemption
        1. Acceptance status
        1. Name of Redemer
        1. Website
        1. Wallet address
        1. Type: Product, Voucher, Cash, Service
        1. Description: 
        1. Currency
        1. Value

# Contract Functions:
    * Issue: Anyone can issue a Cert (this specifies all the informaion and the recipient) - note there is no transfer function. Once issued to the reciever (the owner) they can no longer send it to anyone else. 
    * AcceptCert: The receiver can choose to accept the Certificate 
    * GiveViewAccess: The owner can assign who can view the certificate details
    * RevokeViewAccess: The owner can revoke view access
    * Destroy: The receiver can delete – remove data and send to 0x000
    * Validate/endorse: Anyone can validate a Cert
    * Unvalidate/unendorse: Anyone can revoke their endorsement of a Cert
    * Redeem: Anyone can redeem the cert (note that the certificate stays with the owner)
    * AcceptRedemption: The receiver can accept a redemption. 

# Marketplace:
The certificate owner would have to give the market place or anyone access to view their certificate details.
A market place application should be able to read these certificates and support rating and redemption. 
Certificates can be aggregated and sorted by type, area, gender and so on. 
Recipients can go here to choose to delete their certs, share them with others or approve a redemption

# Risks & Questions
* Someone may potentially recieve many bogus certificates 
   * these should not be 'accepted' by the receiver and can also be Destroyed.
   * Validators should also have certifications 
* Personal Identifying Information written into the Cert is avalible to anyone that can read the Cert.
   * The reciever can have the option to encrypt the data or parts of the data/details
   * Zero knowlege proofs... tbt ... even if someone can't access the data for the credentials or certificate information they could validate it exists.
