# Soft Balance Limits

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Will Ruddick <willruddick@gmail.com> (grassecon.org)
* Date: 2021.5.29
* Version: 1
* Status: Pre-draft

## Rationale
<!--
Why are you proposing this?
-->
1. Some businesses want to ensure they will nto accept too much of a certian token. We want to remind users to not accept more Sarafu than they are willing to and in some cases enforce this. Hence we want to give them the choice to set their own soft limit of how many of a token (i.e. Sarafu) that they have in their wallet.
Users are given a default inital limit of 10,000 (after which there will be an error when trying to add more tokens). 
The user can set this to any nonzero integer - as a profile setting
1. We also wish to be able to enforce a abalance limit on users - similar to volume limits of old

## Before 
<!--
What is the current state of the described topic?
-->
1. no ability to set a max balance limit for the active token - some users get encouraged to collect too many tokens.

## After
<!--
How will things be different after this has been done?
-->
1. ability to set a max balance limit for the Active Token (which can be adjusted). Note that each token can have a differetn balance limit.
1. This can be set in USSD, CICADA as well as CLI


## Implementation
All transactions going to that wallet in that users Active token should check this Balance Limit.
If the transaction will cause the reciever to go over their Balance limit on the Active token a message should be sent to both parties.
Sender "XXX user can't accept more TOKEN_Name please help them spend it"
Reciever "XXX user tried to send YYY TOKEN_NAME but can't because of your Balance Limit of XXX. To change this limit choose My Profile then Balance Limit.
<!--
Here is the description of how these changes should be implemented.
Please use subheadings to improve readability.
Some suggestions:

### Workflow

### Variables

### Interface
-->


## USSD Menu
1. My Profile
  1. Balance Limit
    1. Check Balance Limit (Asks pin and show current limit for active token)
    1. Set Balance Limit (Asks for a non zero integer and a confirmation screen and pin on request)

## Testing
<!--
Please describe what test vectors that are required for this implementation
-->

## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
