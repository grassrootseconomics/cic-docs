# Misc Tech
1. SaaS systems – deployment templates
1. Humanitarian Chain
1. Migration to tendermint (cosmos)
1. Impact Claim (NFTs) creation
1. Zero Knowledge Proofs (anon txns)
1. Decentralized Data Storage (Swarm)
1. Simulation & Modeling (Training & Prediction)
1. Security / Tech Audits
1. Token (CIC) Creation DAO
1. CIC usage (Fund/tax (re)allocation) DAO

# UX
1. Custodial USSD 
1. Custodial Front end (Telegram/Discord)
1. Custodial Front end (Web)
1. Non-Custodial Wallet
1. Market Place (products) Directory
1. Impact Claim (NFT) Market Place
1. Data explorers (Dashboards / Reporting)
1. Social Password Recovery

# Scaling
1. Scaling Plan
1. Operations / Managment
1. Community Management
1. Tech training (on boarding)
1. Implementation Training (Field / Client onboarding)
1. CIC Creation Agreement (text)
1. Library/Docs Coordination (packaging/searchable)

# Partnerships (Decentralized Utility)
1. Mesh Networks
1. IoT / Tokenization
1. Foundational Utilities
1. Infrastructure
1. Agro Forestry / Carbon
