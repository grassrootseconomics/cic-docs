# Environment
deploy contracts
set up system accounts
throw some gas and tokens around for system accounts
start all services

git clone https://gitlab.com/grassrootseconomics/cic-internal-integration/

sudo RUN_MASK=3 COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 DEV_FAUCET_AMOUNT=100000000 TOKEN_TYPE=erc20_demurrage_token CIC_DEFAULT_TOKEN_SYMBOL=SARAFU docker-compose up --build
that should set you up, and then you need to determine when the setup is done by running:

sudo docker logs -f cic-internal-integration_contract-migration_1
when that logs exits, you're good

# Create users
https://gitlab.com/grassrootseconomics/cic-internal-integration/-/tree/lash/migration-fix/apps/data-seeding#alternative-3-ussd-import-cic_ussd


# Token Creation

git clone https://gitlab.com/nolash/grassroots-app


## Setup:
sudo docker-compose down -v

sudo RUN_MASK=3 COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 DEV_FAUCET_AMOUNT=100000000 TOKEN_TYPE=erc20_demurrage_token CIC_DEFAULT_TOKEN_SYMBOL=SARAFU docker-compose up --build

sudo RUN_MASK=3 docker-compose up –build

## Deploy token
erc20-demurrage-token-deploy –help

(venv) wor@gecon:~/src/ge/erc20-demurrage-token/python$ PYTHONPATH=. python erc20_demurrage_token/runnable/deploy.py -p http://localhost:63545 -y /home/wor/src/ge/cic-internal-integration/apps/contract-migration/keystore/UTC--2021-01-08T17-18-44.521011372Z--eb3907ecad74a0013c259d5874ae7f22dcbcc95c -i evm:bloxberg:8996 --name "Chama Token" --symbol "CHM" -vv -ww -c config --supply-limit 1000000

(venv) wor@gecon:~/src/ge/erc20-demurrage-token/python$ eth-get -p http://localhost:63545 0xcf38135da34784a9db226772dcf740089f466241ba82ab4a2f4b5265fcb23a4d 


giftable-token-gift -p http://localhost:63545 -y ./keystore/UTC--2021-01-08T17-18-44.521011372Z--eb3907ecad74a0013c259d5874ae7f22dcbcc95c -i evm:bloxberg:8996 -vv -w -a <token_address> --recipient <recipient_address> <amount>

giftable-token-gift -p http://localhost:63545  -y /home/wor/src/ge/cic-internal-integration/apps/contract-migration/keystore/UTC--2021-01-08T17-18-44.521011372Z--eb3907ecad74a0013c259d5874ae7f22dcbcc95c -i evm:bloxberg:8996 -vv -w -a 0x6ca3cb14aa6f761712e1c18646afba4d5ae249e8 100


(venv) wor@gecon:~/src/ge/erc20-demurrage-token/python$ erc20-balance -p http://localhost:63545 -a 0x6Ca3cB14aA6F761712E1C18646AfBA4d5Ae249E8 0xEb3907eCad74a0013c259D5874AE7f22DcBcC95C
Chama Token (CHM): 0.000100

# Batch Transactions
from: https://gitlab.com/chaintool/chaind-eth/-/tree/lash/sendscript

1. Start or connect to your node: sudo docker-compose up eth  
1. Contract Migration: sudo COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 RUN_MASK=1 docker-compose up --build contract-migration
git clone https://gitlab.com/chaintool/chaind
cd chaind
python -m venv .venv
. .venv/bin/activate
pip install --extra-index-url https://pip.grassrootseconomics.net:8433 -r requirements.txt
# the following will set up your database in ~/.local/share/chaind/eth/chaind.sqlite
PYTHONPATH=. CHAIND_DOMAIN=eth DATABASE_ENGINE=sqlite python scripts/migrate.py #creates the db for the querer syncer and chaind


1. Create a file:
set +a
export DATABASE_ENGINE=sqlite
export RPC_PROVIDER=http://localhost:63545
export RPC_HTTP_PROVIDER=http://localhost:63545
export CHAIN_SPEC=evm:bloxberg:8996
set -a 

1. Start: chaind-eth-server --session-id testsession -vv
1. Start syncer: chaind-eth-syncer
#testing key file with no password
echo
{"address":"eb3907ecad74a0013c259d5874ae7f22dcbcc95c","crypto":{"cipher":"aes-128-ctr","ciphertext":"b0f70a8af4071faff2267374e2423cbc7a71012096fd2215866d8de7445cc215","cipherparams":{"iv":"9ac89383a7793226446dcb7e1b45cdf3"},"kdf":"scrypt","kdfparams":{"dklen":32,"n":262144,"p":1,"r":8,"salt":"299f7b5df1d08a0a7b7f9c9eb44fe4798683b78da3513fcf9603fd913ab3336f"},"mac":"6f4ed36c11345a9a48353cd2f93f1f92958c96df15f3112a192bc994250e8d03"},"id":"61a9dd88-24a9-495c-9a51-152bd1bfaa5b","version":3}
> test.json
export WALLET_KEY_FILE=test.json

#address of the registry for token index (on my computer)
eth-token-index-list -e 0xa579d6AbE59a87f5b23f674cD4967A55754A0864 

# clicada

[meta]
lookup_method = phone
url = https://meta.grassecon.net
http_origin = 

[filestore]
ttl = 86400

[tx]
cache_url = https://cache.grassecon.net
http_origin = 

[cic]
registry_address = 0xcf60ebc445b636a5ab787f9e8bc465a2a3ef8299

[auth]
type = gnupg
db_path = 
keyring_path = /home/wor/src/ge/cic-staff-installer/.gnupg
key = EF26F4682343F692
passphrase = merman

[rpc]
provider = https://rpc.grassecon.net
auth = 
credentials = 
dialect = default
scheme = http

[chain]
spec = evm:byzantium:8996:bloxberg

[wallet]
key_file = 
passphrase =
